# CassandraCheatsheet

## Starting a Cassandra Cluster

**docker-compose up**

- Starts a cluster of 2 nodes

Now you can access one of the nodes with an open sql client by doing:

**docker exec -it cassandracheatsheet_cassandra-node-1_1 cqlsh**

Then you can create your first keyspace, being in the in the cql you can create your first keyspace

```cql
cqlsh>
CREATE KEYSPACE first_example
  WITH REPLICATION = {
  'class' : 'SimpleStrategy',
  'replication_factor' : 2
  };
```

Then, you can also check that number of nodes working to check that all goes good. exit cqlsh console and run

**docker exec -it cassandracheatsheet_cassandra-node-1_1 nodetool status first_example**

