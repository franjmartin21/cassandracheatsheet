# Quick Wins

## nodetool command 

**bin/nodetool status**

- To verify that cassandra is up 

- More information in following lessons about this command

## Keyspaces

This is similar to creating a schema in a relational database

```sql
CREATE KEYSPACE killrvideo
WITH REPLICATION ={
  'class': 'SimpleStrategy',
  'replication_factor': 1
}
```

- Replication parameters are needed when creating a keyspace

**use killrvideo**

- To set the keyspace as the active one when using cqlsh client

## Tables

Keyspaces contain tables

Tables contain data

```sql
CREATE TABLE table1(
    column1 TEXT,
    column2 TEXT,
    column3 INT,
    PRIMARY KEY(column1)
)
```

```sql
CREATE TABLE users(
  user_id UUID,
  first_name TEXT,
  last_name TEXT,
  PRIMARY KEY(user_id)
)
```

- text datatype, would be like varchar, it is encoded in UTF-8

## UUID AND TIMEUUID

Used in place of integer IDs because Cassandra is a distributed database

### UUID

- Universally unique identifier

- Generated via uuid() method

### TIME UUID embeds a TIMESTAMP value

- It is sortable

- generate via now()

## INSERT

```sql
INSERT INTO users(user_id, first_name, last_name)
VALUES(uuid(), 'Joseph', 'Chu');
```

- Very similar syntax to a relational db


## SELECT

```sql
SELECT *
FROM users
WHERE user_id = e6dc6f27-48d2-451c-9a02-bf7e45db94db;
```

- Very similar syntax to relational db

## COPY

imports/exports CSV

```sql
COPY table1(column1, column2, column3) FROM 'table1data.csv'
```

```sql
COPY table1(column1, column2, column3) FROM 'table1data.csv'
WITH HEADER=true
```

- COPY is one of the best ways to ingest data into cassandra


# Partitions

Partition key is what defines how your data is going to be partitioned

Partitioner decides where each tuple will go

First field in the PRIMARY KEY is always the PARTITION KEY

ie. PRIMARY KEY((state), id)

- state would be the PARTITION KEY

- Because if the PK only were state then we could just have 50 different rows

Cassandra uses consistent hashing for the partition key to make sure it goes to the right node in the partition ring


# Clustering Columns

## Basic rules

Together with partition keys is the most important thing of making cassandra efficient


You cannot change a primary key on the table once it has been created so we need a different solution if we want to sort by different criteria

Clustering columns are all the rest of the primary keys in the table after the first one 

ie
**PRIMARY KEY ((state), city, name)**

- state would be the partition key

- city and name would be the clustering key

Custering columns are going to define the order of the data in disk

In the previous example the primary key chosen could not ensure uniqueness, we can add at the end the id to ensure it

**PRIMARY KEY ((state), city, name , id)**

## Querying Clustering Columns

Every query should have a partition key

- That will indicate cassandra where the data that you are looking for is

Once you have provided the partition key You can perform either equality(=) or range queries (<, >)on clustering columns

You need to apply filters in the right order defined of clustering columns

## Changing the default ordering

By default clustering columns are stored in ascending order

WITH CLUSTERING ORDER BY allows to change this

ie.
```sql
CREATE TABLE users(
  state text, 
  city text, 
  name text, 
  id uuid,
  PRIMARY KEY((state), city, name, id))
  WITH CLUSTERING ORDER BY(city DESC, name ASC)
```

- as id is not included, it is assumed ASC

This feature is useful for time series

## Allow Filtering

Used when you need to scan more than one partition in your query

You can then query on just clustering columns

It will make cassandra to scan all partitions in the table

DON'T USE unless:

- You really have to

- You have small dataset

- Strongly recommended to not use it


# Node

Runs in a server or VM and runs on JVM

Could live in a cloud on in a datacenter

Don't run it on a SAN (Don't use when disk is connected with an ethernet cable)

Node is responsible for the data that it contains which depends on the hash of the partition key

Node can support

- 6000 - 12,000 transactions/second/core

- 2 - 4 Terabytes on SSD

Nodetool can be used to query for information about the nodes of the cluster

**nodetool help**

- See information about the command

**nodetool info**

- Information about the node on the host where you run the command

**nodetool status**

- Information about the nodes of the cluster


# Ring

Adding more nodes (scaling out) is how cassandra deals with increases of load

Cassandra disposes the nodes in ring

The node that recieves a request is called, the coordinator node

When receiving a new call the coordinator node sends the data for the right node to respond to the call

Any node can act as coordinator

## Partitioner

Determines how the data is going to be distributed accross the ring


## Node Joining the cluster

1. Node joins the cluster by communicating with any node

2. Apache Cassandra finds these seed nodes list of possible nodes in cassandra.yaml

3. Seed nodes communicate cluster topology to the joining node

4. Oce the node joins the cluster, all nodes are peers.


## Drivers

Drivers intelligently choose which node would best coordinate a request

Per-query basis

**ResultSet results = session.execute("\<query\>");**

TokenAwarePolicy - driver chooses node which contains the data

RoundRobinPolicy - driver rounds robins the ring

DCAwareRoundRobinPolicy - driver round robins the target data center


# Peer to Peer

No leaders and followers. In cassandra there are replicas and all are the same

Coordinator is able to send the request to all the nodes where there is a replica of the data requested

Even if the ring splits in two halves, each half can continue serving queries

 
# VNodes

In a cassandra cluster we might need to add or remove nodes.

Good partitioner should spread tokens evenly across nodes

When we add a new node, apache cassandra needs to move the data to new node. However this operation could add stress to a node already stressed.

VNodes help to keep the cluster balanced as new nodes and old nodes are added or removed.

### VNode details

- Adding/Removing nodes with vnodes helps keep the cluster balanced

- By default, each node has 128 vnodes

- VNodes automate token range assignment


### Configuration

- Configure vnode settings in cassandra.yaml

- num_tokens setting

- Value greater than one turns on vnodes


# Gossip

Gossip is the broadcast protocol for disseminating data

### Choosing a Gossip Node

Each node initiates a gossip round every second

Picks one to three nodes to gossip with

Nodes can gossip with ANY other node int the cluster

Probabilistically (slightly favor) seed and downed noedes

Nodes do not track which nodes they gossiped with prior

Reliably and efficiently spreads node metadata through the cluster

Fault tolerant--continues to spread when nodes fail


### What data Gossip spreads

Only spreads Node metadata not client data

Each node has and "Endpoint State", stores all the gossip information for a single node

* Heartbeat State: Tracks 2 values
    * generation - Timestamp of when the node bootstrapped.
    * version -  It is a simple integer. Each node increments its value every second.
* Application State: Metadata for this node. Data about the node that gossip spreads in the cluster
    * STATUS - NORMAL|LEAVING|LEFT|REMOVING|REMOVE - Nodes declare their own status
    * DC -Node data center
    * RACK - Rack of the node
    * SCHEMA - Changes as the node changes over time
    * LOAD - Disk space usage
    * ...
    
### How Gossip works.

Simplifiying a lot:

- One node starts communication with another node with a SYN message. Communicating the version that it has of each node

- Second node compare versions with its versions for each node, and ask for updates for versions that it may have stale

- First node sends updates of those nodes

### Network Traffic

Not heavy amount of network traffic:

- Constant rate (tickle) of network traffic

- Minimal compared to data streaming, hints

- Doesn't cause network spikes


# Snitch

Determines / declares each node's rack and data center

Determine the topology of the cluster

Several types of snitches

Configured in cassandra.yaml file

###Types of Snitches

- Regular
    - SimpleSnitch: Is default. Places all nodes in dc1 and rack1
    - PropertyFileSnitch: Retrieves DC AND RACK assignments from a configuration file
    - GossipingPropertyFileSnitch: Configure each individually and then gossip just spreads that information. You don't need to maintain a copy of settings in each node as PropertyFileSnitch
    - RackInferringSnitch: Infers the DC, rack and node from the ip address

- Cloud-Based Snitches
    - Ec2Snitch: Single regison Amazon EC2 deployment
    - Ec2MultiRegionSnitch: Multi-region EC2 cloud deployment
    - GoogleCloudSnitch: Multi-region cloud Google deployment
    - CloudstackSnitch: For Cloudstack envs
    
- Dynamic Snitch 
    - Is layered on top of actual snitch
    - Maintains a pulse on each node's performance
    - Determines which node to query replicas from depending on node health
    - Turned on by default for all snitches
   
   
### Configuring Snitches

- All nodes in the cluster must use the same snitch

- Changing cluster network topology requires restarting all nodes

- Run sequential repair and cleanup on each node

# Replication

RF= replication factor determines how many copies of data are going to be maintained 

RF=3 Is a good number

### During a failure

It is going to make node failures much less of an impact as we main copies of the data in multiple nodes

### Mutiple Data Center Replication 

It uses the snitch which is topology aware to make sure data is replicated

Uses Keyspace declaraion to know how it should be replicated
```sql
CREATE KEYSPACE killrvideo
WITH REPLICATION = {
  'class': 'NetworktopologyStrategy',
  'dc-west': 2,
  'dc-east': 3
}
```
How replication works in this case:

- Coordinator of one dc gets the data

- It seds a message to the right nodes within its datacenter to store that data

- It also sends a message to coordinator in the other DC to store that data

- Coordinator in second data center sends the data to the right nodes within its datacenter


# Consistency

Cassandra is actually optimized for availability and partition at the expense of consistency

Consistency is managed at the write level operation

Two main things to take into account:

- RF As you know is the replication factor

- CL is the consistency level

Three possible levels of CL

- CL=ONE - Only one replica needs to acknowledge that the write happened - FASTEST BUT LESS STRICT CONSISTENCY LEVEL

- CL=QUORUM - At least half + 1 nodes to acknowledge the correct write - SLOWER BUT MORE CONSISTENT

- CL=ALL - All replicas need to acknowledge - NOT RECOMMENDED

### Strong consistency

It is to ensure that the data that you wrote in, is the data that you are going to read, and not a previous state.

- CL=ALL - Would ensure this, but at the expense of availability so we need a better solution

- QUORUM for READ and WRITES - It would also provide a good consistency guarantee without compromising availablity that much

- CL=ONE - When consistency is not that important and the most important thing is speed.

### Across Data Centers

QUORUM would mean QUORUM on all data centers which will introduce a lot of latency in the system

LOCAL_QUORUM: Only looking for QUORUM consistency in the local data center

### Consistency Levels from the Weakest to the Strongest

ANY: (Not really used) Storing a hint at minimum is satisfactory

ONE, TWO, THREE: Checks closest node(s) to coordinator

QUORUM: Majority vote, (sum_of_replication_factors / 2) + 1

LOCAL_ONE: Closest node to coordinator in same data center

LOCAL_QUORUM: Closest quorum of nodes in same data center

EACH_QUORUM: Quorum of nodes in each data center, applies to writes only

ALL: Every node must participate - Very strict


### Consistency Settings

The higher the consistency, teh less chance you may get stale data

More consistency is more latency

Consistency chosen depends on your situational needs

# Hinted Handoff

It is the way used by Cassandra of repeating write calls from the coordinator to a node that was down

Cassandra stores hints for this

### Settings

cassandra.yaml stores configuration of hints

You can disable hinted handoff

choose directory to store hints file

set the amount of time a node will store a hint

default is three hours

### Beware consistency level ANY

Consistency level ANY means that storing hint suffices

Consistency level one or more means at least one replica must successfully write

Hint does not suffice

If hint time expires the coordinator will delete the write

If coordinator dies you lose the write too

# Read Repair

Over time nodes can get out of sync

### Anti-Entropy Operations

Network partitions cause nodes to get out of sync

You must choose between availability vs. consistency

CAP Theorem

### Normal Read

Assuming a CL = ALL

1. Coordinator gets a request.

2. Coordinator requires the data to the most available node

3. Coordinator also requires the checksum to the other nodes that store that data

4. Coordinator computes the checksum of the data and if it matches, it returns the data to the client.

Assuming the checksum don't match

1. Same process that described before

2. This time the checksum don't match - This means that something is out of sync

3. Coordinator then is responsible of checking the timestamp to figure out what is most recent data. For that, it will require the TS of the replicas

4. Then, the coordinator determines which data is the most recent and sends that data to the replicas with the old data so they can get their data on Sync

5. Simultaneously to 4, coordinator returns the data to the client

### Read Repair Chance

When read is at a consistency level less than all:

- Request reads only a subset of the replicas

- We can't be sure replicas are in sync

- Generally you are safe, but no guarantees

- Response sent immediately when consistency level is met, and read repair done asynchronously in the background

- dclocal_read_repair_chance set to 0.1 (10%) by default
    - Read repair that is confined to the same datacenter as teh coordinator node
    
- read_repair_chance set to 0 by default
    - For a read repair across all datacenters with replicas


### Nodetool Repair

Syncs all data in teh cluster

It is expensive and grows with amount of data in cluster

Use with clusters servicing high writes/deletes

Last line of defense

Run to Sync a failed node coming back online

Run on nodes not read from very often


# Node Sync

### Full repairs

Full repairs bog down the system

Bigger the cluster and dataset, the worse the time

In times past, it was recommended to run full repair within gc_grace_seconds.

### Node Sync

New feature just in Datastax enterprise

Runs in background continuously repairing your data

Better to repair in small chinks as we go rather than full repair

Automatically enabled by default

Then, it has to be enabled per table

```sql
CREATE TABLE myTable (...)
WITH nodesync = {'enabled': 'true'}
```

# Write Path

- When new data is writen into a node in cassandra. Two things happen
    - COMMIT LOG: The new row is appended into a log file called the COMMIT LOG, in the SSD storage
    - MEM TABLE: The new row is added into what it is called the MEM TABLE which lives in RAM memory. This data is sorted by dist key.
    
- When memory is read, it will be read from the MEM TABLE

- If the node goes down, Cassandra can restore the MEM TABLE by playing in order the rows in the COMMIT LOG

- SSTable: When MEM TABLE is full, we need to flash it to the hard drive. We flash it into SSD in what it is called a SSTable (Sorted String Table)
    - SSTable is immutable
    - If MEM TABLE fulls again, a new SSTable will be written in SSD
    
- It is recommended to have the SSTable in a different HD than the COMMIT LOG


# Read Path

- Reading from the MemTable
    - Looks up the partition with the partition token, basically a Binary Search.

- Reading from SSTable.
    - They have file offsets, to easily find each partition in the SSTable
    - Every SSTable contains a partition index
    - Cassandra also build a PARTITION SUMMARY which leaves in RAM, To speed up the process of finding the element in the SSTable partition index
    - Cassandra stores the result of previous reads in a key cash to speed up process when same partitions keys are retrieved 
    
- Bloom Filter
    - Gives a response just about if a partition key is there or not. 
    - If Bloom Filter don't know it will need to go through previous process to find out
 
- DataStax Enterprise has some read optimizations
    - No Partition Summary 
    - Partition index changed with a trie-based data structure
    - Big performance improvements for large SSTables
 
# Compaction

Removes stale data from the pre-existing SSTables

Compaction is the process of merging all SSTables and its related structures (Bloom filter, Partition summary, index and SSTable itself)

### Compacting Partitions

- If the key is just inone of the SSTables it simply gets copied to the new compacted SSTable 

- If the key is in both SSTables, cassandra compares the values in SSTables using the timestamp to decide which value is more recent

- When deleting in Cassandra, it writes a new value called the Tombstone, Tombstone will make the record be deleted when compacting SSTables

- Keys, with Tombstones are kept if time since that Tombstone was added is less than certain limit. This is done to prevent resurrection of that value in a process of recovery


### Compacting SSTables

- It performs like a merge operation partition by partition.

- When a partition is in both SSTables then it will go through the "Compacting Partitions" process decribed above

- The resulting partition is writen in a new SSTable

- The resulting partition could be smaller than the original partions being merged if there is a number or records marked with Tombstone

### Compaction Strategy

This is for cassandra to choose when to do compaction and what SSTables need to be compacted

This strategies are configurable and include:

- SizeTiered Compaction: Multiple SSTables of a similar size are present

- Leveled Compation: Groups SSTables into levels, each of chich has afixed size limit which is 10 times larger than previous level

- TimeWindow Compaction - Creates time windowed buckets of SSTables that are compacted with each other using the Size Tiered Compaction Strategy

Alter table to change strategy

```sql
ALTER TABLE mykeyspace.mytable WITH compaction = {'class': 'LeveledCompactionStrategy'};
```

# Advanced Performance

Datastax 6 enterprise contains enhancements that allow to scale up in a single machine by adding cores

- One thread per core


    
    
    



