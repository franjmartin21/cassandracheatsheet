# Relational vs. Apache Cassandra

Cassandra supports a denormalized model where tables are structured around the queries that are going to be run in the DB. Comparison

See below what would be a query in traditional relational model
```sql
SELECT comment
FROM videos JOIN comments
ON videos.id = comments.video_id
WHERE title = 'Interestellar'
```

- This query joins two tables to get the comments by video title

If we want to get by author

```sql
SELECT comment
FROM videos JOIN comments
ON videos.id = comments.video_id
WHERE author = 'Author'
```

- Again we join the table to get the comments of a specific author

In Cassandra, we would have 2 tables like

```sql
CREATE TABLE comments_by_video(
  video_title text,
  comment_id timeuuid,
  user_id text,
  video_id timeuuid,
  comment text,
  PRIMARY KEY ((video_title), comment_id)
);

CREATE TABLE comments_by_user(
  user_login text,
  comment_id timeuuid,
  user_id text,
  video_id timeuuid,
  comment text,
  PRIMARY KEY ((user_login), comment_id)
);
```

- So in each case, we choose as primary key the field that we are filtering on

- We are also using cassandra naming convention

In cassandra, referential integrity is not guaranteed by the database, we need to enforce it at app level:

- Due to performance reasons, we would need read before write

- You can run DSE Analytics with Apache SparkTM to validate that duplicate data is consistent

# Working with KillrVideo

KillrVideo Challenges

- Scalability: More and more users and videos

- Reliability: Needs to be always available

- Easy to use: Needs to be easy to manage and maintain


Currently in relational database

- Single points of failure

- Scaling complexity

- Reliability issues

- Difficult to serve users worldwide

# Quickstart on CQL
 
### Keyspaces

- Top-level namespace/container

- Similar to a relational database schema

- Replication parameters required

```sql
CREATE KEYSPACE killrvideo
WITH REPLICATION = {
 'class': 'SimpleStrategy',
 'replication_factor': 1
}
```

USE COMMAND

- Select the keyspace to work on

```sql
USE killrvideo;
```

### Tables

- Keyspaces contain tables and tables contain data

```sql
CREATE TABLE table1(
  column1 TEXT,
  column2 TEXT,
  column3 TEXT,
  PRIMARY KEY(column1);
);
```

```sql
CREATE TABLE users(
  user uuid,
  email text,
  name text,
  PRIMARY KEY(user)
);
```

Basic data types:

- Text
    - UTF8 encoded string
    - varchar is same
- Int
    - Signed
    - 32 bits   
- UUID
    - generate via uuid()
- TIMEUUID embedas a TIMESTAMP value
    - Sortable
    - Generate via now()
- Timestamp
    - date and time
    - 64bit integer
    - Milliseconds since Jan 1st 1970 at 00:00:00 GMT
    - Displayed in cqlsh as yyyy-mm-ss HH:mm:ssZ
    
### COPY command

- Imports/export CSV

```sql
COPY table1(column1, column2, column3) FROM 'table1data.csv';
```

```sql
COPY table1(column1, column2, column3) FROM 'table1data.csv'
WITH HEADER = true;
```


### SELECT

```sql
SELECT column1, column2, column3
FROM table1;
```

```sql
SELECT *
FROM table1
LIMIT 10;
```

### TRUNCATE

- Removing table data

- Truncate sends a JMX command to all nodes to delete   SSTables that hold teh data

- If any of thses nodes are down the command will fail

```sql
TRUNCATE table1;
```

### ALTER TABLE

Change things of the estrucure of the table:

- Type of a colummn, add columns, drop columns, rename columns, change table properties

- You cannot alter the PRIMARY KEY columns

```sql
ALTER TABLE table1 ADD another_column TEXT;

ALTER TABLE table1 DROP another_column;
```

### SOURCE

Executes CQL scripts

- Executes a file containing CQL statements

- Enclose file name in single quotes

- Output for each statement appears in turn

```sql
source './myscript.cql'
```

# Fundamentals of an Apache Cassandra Table

### Terms and Definitions

Data Model:

- Abstract model for organizing elements of data

- Cassandra is based on queries you want to perform

Keyspace: 

- Similar to schema in relational databases

- All tables live in a keyspace

- Keyspace is the container for the replication

Table:

- Grouped into keyspaces

- Contain columns

Partition:

- Row(s) of data that are stored on a particular node in your based on a partitioning Strategy


### Specific to the tables

Row:

- One or more CQL rows stored together on a partition

Column:

- Similar to column in a relational database

- PK: Used to access the data in a table and guarantees uniqueness

Partition Key: Defines the node on which the data is stored

Clustering column:

- Defines the order of rows within a partition

### Cassandra Data Types

Text data types:

- Ascii: US-ASCII characters

- Text: UTF-8 encoded string

- Varchar: UTF-8 encoded string

Numeric data types:

- Tinyint: 8-bit signed integer

- Smallint: 16-bit signed

- Int: 32-bit signed integer

- Bigint: 64-bit signed integer

- Varint: Arbitrary-precision integer -- F-8 encodeed string

- Decimal: Variable-precision decimal, supports integers and floats.

- Float: 32-bit IEEE-754 floating point

- Double: 64-bit IEEE-754 floating point

Time, Timestamp and unique identifiers

- Date: 32-bit unsigned integer -- Number of days since epoch

- Duration: Signed 64-bit integer -- amount of time in nanoseconds

- Time Encoded 64-bit signed--number of nanoseconds since midnight

- Timestamp: 64-bit signed integer--date and time since epoch in milliseconds

- UUID: 128 bit universally unique identifier -- generate with the UUID function

- TimeUUID: unique identifier that includes a conflict-free timestamp -- generate with the the NOW function

Some other data types that don't fit in prior categories

- Blob: Arbitrary bytes, expressed as hexadecimal

- Boolean: Stored internally as true or false

- Counter: 64-bit signed integer -- only one counter column is allowed per table

- Inet: IP address string in IPv4 or IPV6 format

# Partitioning and Storage Structure

### Queries in relational context

In cassandra, traditional relational queries like

```sql
SELECT *
FROM videos
WHERE added_date < '2015-05-01'
```

Would return:

**InvalidRequest: code=200 [Invalida query] message="No secondary indexes...**


### Cassandra uses Partition Storage

WHERE on non-primary key columns

- Cassandra distributes partitions across nodes

- WHERE on any field other than partition key would require scan of all partitions

- Inefficient access pattern

WHERE on partition key values

- We can WHERE on a partition key value sas well as clustering columns

- Cassandra uses a hashing algorithm to quickly determine with node(s) contain the desired partition


### Primary Key

Simple Primary key

- Contains only the partition key

- Determines which node stores the data


Composite Primary key

```sql
CREATE TABLE videos(
  name text,
  runtime int,
  year int,
  PRIMARY KEY ((name, year))
);
```

- Double parenthesis because you want both name and year to be used for the partition key

- It is good if you know that you are always going to use the 2 columns in queries.

- You are also forcing you to always use both columns in the WHERE clauses.

### Primary key vs. Partition Key

Partition key: The part of the primary key that determines what node the partition is stored on

Primary key: Includes partition key and any/all clustering columns


# Clustering Columns

Clustering columns is how cassandra sorts data within each partition

- Clustering columns come after partition key in the PRIMARY

```sql
CREATE TABLE videos(
  id int,
  name text,
  runtime int,
  year int,
  PRIMARY KEY((year), name)
);
```

So cluster columns impact in the order in which each row will be stored

You can also define if order will be ascending or descending

```sql
CREATE TABLE videos(
  id int,
  name text,
  runtime int,
  year int,
  PRIMARY KEY ((year), name)
) WITH CLUSTERING ORDER BY (name DESC);
```

- name will be ascending

Then you can query clustering columns because lookup is fast

```sql
SELECT *
FROM videos
WHERE year =2014 AND name = 'Mockingjay';
```

- Cassandra is able to easily find the partition with the year filter

- Then, in the correct partition it easily localizes the name as it is sorted

If you have to use clustering columns in your query, you need to follow in your query the order in which you declared them in the PK

# Denormalization

Example of relational query

```sql
SELECT comment
FROM videos
JOIN comments on videos.id = comments.video_id
WHERE title = 'Interstellar'
```

- If there a lot of joins and records on each table, eventually queries will get slow.

You can denormalize the data to speed up queries

- The trade-off is that when you denormalize you will have duplicated data

Having denormalized tables that fit the way the data is going to be queried is how things should be done in Cassandra:

```sql
CREATE TABLE comments_by_video(
  video_title text,
  comment_id timeuuid,
  user_id text,
  video_id timeuuid,
  comment text,
  PRIMARY KEY((video_title), comment_id)
);
```

```sql
CREATE TABLE comments_by_user(
  user_login text, 
  comment_id timeuuid,
  user_id text,
  video_id timeuuid,
  comment text,
  PRIMARY KEY ((user_login), comment_id)
);
```

# Collections

Group and store data together in a column

- Collection columns are multi-valued columns

- Designed to store a small amount of data

- Retrieved in its entirety

- Cannot nest a collection inside another collection, unless you use FROZEN keyword

### Set

- Typed collection of unique values

- Stored unordered, but retrieved in sorted ordered

Set Example

```sql
CREATE TABLE users(
  id text PRIMARY KEY,
  fname text,
  lname text,
  emails set<text>
);
INSERT INTO users(id, fname, lname, emails)
VALUES ('cass123', 'Cassandra', 'Dev', {'cass@dev.com','cassd@gmail.net'});
```

### List

- Like SET, collection of values in the same cell

- Do not need to be unique and can be duplicated

- Stored in a particular order 

```sql
ALTER TABLE users ADD freq_dest list<text>;

UPDATE users SET freq_dest= ['Berlin', 'London', 'Paris']
WHERE ID ='cass123';
```


### Map

- Typed collection of key-value pairs. Name and pair of typed values

- Ordered by unique keys

- MAP example: 

```sql
ALTER TABLE users ADD todo map<timestamp, text>;

UPDATE users SET todo = {'2018-1-1': 'create database', '2018-1-2': 'load data and test', '2018-2-1': 'move to producction' }
```

### Using FROZEN into a Collection

- If you want to nest datatypes, you have to use FROZEN

- Using FROZEN in a collection will serialize multiple components into a single value

- Values in a FROZEN collection are treated like blobs

- Non-frozen types allow updates to individual fields


# UDTs

UDT stands for User Defined Types

- User-defined types group realted fields of information

- User-defined types (UDTs) can attach multiple data fields, each named and typed, to a single column

- Can be any datatype including collections and other UDTs

- Allows embedding more complex data within a single column


Defining two new types:

```sql
CREATE TYPE address(
  street text,
  city text,
  zip_code int,
  phones set<text>
);

CREATE TYPE full_name(
  first_name text,
  last_name text
);
```

```sql
CREATE TABLE users (
  id uuid,
  name frozen <full_name>,
  direct_reports set<frozen<full_name>>,
  addresses map<text, frozen<address>>,
  PRIMARY KEY ((id))
);
```



# Counters

- Column used to store a 64-bit signed integer

- Changed incrementally, incremented or decremented

- Values are changed using UPDATE

- Need specially dedicated tables, can only have primary key and counter columns


Example of usage of counter:
```sql
CREATE TABLE moo_counts(
  cow_name text,
  moo_count counter,
  PRIMARY KEY((cow_name))
)
```

Then, when we use the counter:

```sql
UPDATE moo_counts
SET moo_count=moo_count + 8
WHERE cow_name = 'Betsy'
```

### Counter considerations

- Distributed system can cause consistency issues with counters in some cases

- Cannot INSERT or assign values. Default value is "0"

- Must be only non-primary key columns

- Not idempotent

- Must use UPDATE command. DataStax Enterprise rejects USING TIMESTAMP or USING TTL to update counter columns

- Counter columns cannot be indexed or deleted


#UDFs and UDAs

### User Defined Functions

- Functions in Java or Javascript

- Use SELECT, INSERT and UPDATE statements for using this functions

- Functions are only available in the keyspace where you created them

### Creating UDFs Syntax

- You need to enable them by changing the following settings in `cassandra.yaml` file

    - Java: Set `enable_user_defined_functions` to true
    
    - Javascript and other custom languages: Set `enable_scripted_user_defined_functions` to true
    
## User Defined Aggregates (UDAs)

- DataStax Enterprixe allows userss to define aggregate functions

- Functions are applied to data stored in a table as part of a query result

- The aggregate function must be created prior to its use in a SELECT statement

- Query must only include the aggregate function itself, no additional columns


### Creating the UDF AND UDAs

Example UDF:
```sql
CREATE OR REPLACE 
    FUNCTION avgState(state tuple<int, float>, val float)
    CALLED ON NULL INPUT
    RETURNS tuple<int,float>
    LANGUAGE java
    AS 'if(val != null){
      state.setInt(0, state.getInt(0) + 1);
      state.setFloat(1, state.getFloat(1) + val.floatValue());
    }
    return state;'
```
```sql
CREATE OR REPLACE
    FUNCTION avgFinal(state tuple<int, float>)
    CALLED ON NULL INPUT
    RETURN float 
    LANGUAGE java
    AS 'float r = 0; 
    if(state.getInt(0) == 0) return null;
      r = state.getFloat(1)
      r/=state.getInt(0);
    return Float.valueOf(r);';
```

Create UDA Example:

```sql
CREATE AGGREGATE
    IF NOT EXISTS average (float)
    SFUNC avgState
    STYPE tuple<int, float>
    FINALFUNC avgFinal
    INITCOND (0,0)
```

### Querying with a UFD and a UDA

- The state function is called once for each row

- The value returned by the state function becomes the new state

- After all rows are processed, the optional final function is executed with the last state value as its argument

- Aggregation is perform by the coordinator

```sql
select average(avg_rating) from videos where release_year = 2002 ALLOW FILTERING;
select average(avg_rating) from videos where title = 'Planet of the Apes' ALLOW FILTERING;
select average(avg_rating) from videos where mpaa_rating = 'G' ALLOW FILTERING;
select average(avg_rating) from videos where genres contains 'Romance' ALLOW FILTERING;
```


# Conceptual Data Modeling

- Abstract view of your domain 

- Technology independent

- Not specific to any database system

Purpose of Conceptual Modeling

- Understand your data

- Essential objects

- Constraints

Anyone could be involved in the Conceptual Data Modeling

1. Identify the Key Attributes

2. Find composite attributes

3. Multi-valued attributes

4. Entity-Relationship (ER) Model

5. Identify Entities

6. Identify Relationships

7. Cardinality: Number of times an entity can/must participate in the relationship

8. Weak entity Types: It's existance is dependant of another entity


# Application Workflow and Access Patterns

Apache Cassandra is query driven so we need to figure out the queries early in the process

Application workflow:

- Each application has a workflow - Tasks/casual dependencies form a graph

- Access patterns help determine how data is accessed, Know what queries you will run first

### Example application Workflow

1. User lLogs into site: Find a user with a specified email

2. Show latest videos added to the site: Find most recently uploaded videos

3. Show basic information about a user: Find a user with a specified id

4. Find videos uploaded by a user with a known id(show most recently uploaded videos first)

5. Show a video and its details: Find a video with a specified video id.

# Mapping Conceptual To Logical Model

Logical Data model comes from:

- Conceptual Data model 

- Access Patterns (Queries)

- We apply Mapping Rules and Patterns

### Chebotko Diagrams

- Graphical representation of Apache Cassandra database 

- Documents the logical and pysical data model

![ChebotkoDiagram](ChebotkoDiagram.png)

### Chebotko Diagram Notation

![ChebotkoDiagramNotation](ChebotkoDiagramNotation.png)


### Logical UDT Diagram

- Represents user defined types and tuples

- It is like a table without PK

- You can nest it into a table


### Physical UDT Diagram

The Physical diagram is like logical but adding the data types

![ChebotkoPhysicalDiagram](ChebotkoPhysicalDiagram.png)


### Example complete Chebotko Diagram

![ChebotkoCompleteDiagram](ChebotkoCompleteDiagram.png)


### Data Modeling Principles

- Know your data

- Know your queries

- Nest data

- Duplicate Data


### Single Partition Per Query

- This is the most efficient access pattern for Apache Cassandra

- Query accesses only one partition to retrieve results

- Partition can be single-row or multi-row

### Partition+ Per Query

- Less efficient access pattern but not necessarily bad

- Query need to access multple partitions to retrieve results

### Table Scan/Multi-Table Scan 

- This is the Anti-pattern you need to avoid

- Least efficient type of query but may be needed in some cases

- Query needs to access all partitions in a table(s) to get results


# Logical Data Model

### We want to Nest Data as much as possible

- Nesting organizes multiple entities into a single partition

- Supports partition per query data access

- Three data nesting mechanisms:

    - Clustering columns - Multi-row partitions
    
    - Collection columns
    
    - User-defined type columns
    
### Nest Data - Clustering Columns

- Partition key identifies an entity that other entities will nest into

- Values in a clustering column identify the nexted entities

- Multiple clustering columns implement multi-level nesting

### Nest Data - UDT

- Represents one-to-one relationship, but can use in conjuction with collections\

- Easier than working with multiple collection columns

### Duplicate Data

- Partition per query and data nesting my result in data duplication

- Query results are pre-computed and materialized

- data can be duplicated across tables, partitions and/or rows

Data duplication scale, joins cannot

Datastax has tools to help with duplication problems


### Mapping Rules

- Mapping rules ensure that logical model is correct

- Each query has a corresponding table

- Tables are designed to allow queries to execute properly

- Tables return data in the correct order

What are the Mapping Rules?

1. Entities and relationships

2. Equality search attributes

3. Inequality search attributes

4. Ordering attributes

5. Key attributes

![ApplyingMappingRules](ApplyingMappingRules.png)

# Physical Modeling

Adding data types and creating tables

![CreatingPhysicalModel](CreatingPhysicalModel.png)

Then, we can create our table statements

```sql
CREATE TABLE comments_by_user(
  user_id UUID,
  posted_timestamp TIMESTAMP,
  video_id TIMEUUID,
  comment TEXT,
  title TEXT,
  type TEXT,
  tags SET<TEXT>,
  preview_thumbnails MAP<INT, BLOB>,
  PRIMARY KEY ((user_id), posted_timestamp, video_id ASC)
);
```

Then, you need to lad data. Methods available:

- COPY command

- SSTable loader

- DSE Bulk Loader

### Copy Command

- COPY TO exports data from a table to a CSV file

- COPY FROM imports data to a table from a CSV file

- The process verifies the PRIMARY KEY and updates existing records

- if HEADER = false is specified the fields are imported in that order -- missing and empty fields set to null

- Source cannot have more fields than the target table -- can have fewer fields

```sql
COPY table1 (column1, column2, column3) FROM 'table1data.csv'
WITH HEADER=true 
```

### SSTable Loader 

- Bulk load external data into a cluster

- Load pre-existing SSTables into

    - an existing cluster or new cluster
    
    - a cluster with the same number of nodes or a different number of nodes
    
    - a cluster with a different replication strategy or patitioner
    
Example:

`sstableloader -d 110.82.155.1 /var/lib/cassandra/data/killrvideo/users`

### DSE Bulk Loader

- Moves Cassandra data to/from files in the file system 

- Uses both CSV or JSON formats

- Command-line interface 

- Used for loading large amounts of data fast

Example:

`dsbulk load -url file1.csv -k ks1 -t table1`

### Analysis and Validation

Some important considerations:

- Natural or surrogate keys?

- Are write conflicts (overwrites) possible?

- What data types to use?

- How large are partitions?

- How much data duplication is required?

- Are client-side joins required and at what cost?

- Are data consistency anomalies possible?

- How to enable transactions and data aggregation?












